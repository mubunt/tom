# RELEASE NOTES: *tom*, A command-line utility to generate a self-extractable archive.

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.1.7**:
  - Updated build system components.

- **Version 1.1.6**:
  - Updated build system.

- **Version 1.1.5**:
  - Removed unused files.

- **Version 1.1.4**:
  - Updated build system component(s)

- **Version 1.1.3**:
  - Reworked build system to ease global and inter-project updated.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.1.2**:
  - Some minor changes in .comment file(s).

- **Version 1.1.1**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

- **Version 1.1.0**:
  - Redesigned application, now based on libTom library.

- **Version 1.0.5**:
  - Fixed ./Makefile and ./src/Makefile.
  - Removed some compilation warnings in ./src/tom.c and ./src/tom_funcs.c (still remain 1).

- **Version 1.0.2**:
  - Added a progress bar to show the progress of the copies of the files (This replaces the dots).

- **Version 1.0.1**:
  - Put in comments all the calls related to the trace (*tracy* package).

- **Version 1.0.0**:
  - First version.
