//------------------------------------------------------------------------------
// Copyright (c) 2018-2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: tom
// A command-line utility to generate a self-extractable archive
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <libgen.h>
#include <stdlib.h>
#include <unistd.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "tom.h"
#include "tom_cmdline.h"
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
struct gengetopt_args_info	args_info;
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	struct s_archive archiveParam;
	int errorCode, report;
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_tom(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	//----  Go on --------------------------------------------------------------
	archiveParam.archiveName = args_info.output_arg;
	archiveParam.applicationName = args_info.name_arg;
	archiveParam.applicationRevision = args_info.revision_arg;
	archiveParam.applicationDate = args_info.date_arg;
	archiveParam.applicationPath = args_info.path_arg;
	archiveParam.applicationDescription = args_info.text_arg;
	archiveParam.applicationLicenseFile = args_info.license_arg;
	archiveParam.applicationPostInstallationScript = args_info.script_arg;
	archiveParam.applicationFreeLicence = args_info.free_given;

	if ((report = tom_genarchiv(archiveParam, NOSILENT, &errorCode)) == EXIT_SUCCESS) {
		fprintf(stdout, "\nSelf extractable archive '%s' successfully generated.\n\n", archiveParam.archiveName);
	} else {
		fprintf(stdout, "\nERROR %d: %s\n\n", errorCode, tom_latesterror());
	}
	//---- Exit ----------------------------------------------------------------
	cmdline_parser_tom_free(&args_info);
	return report;
}
//------------------------------------------------------------------------------
