 # *tom*, a command-line utility to generate a self-extractable archive.

**tom** is a command-line utility running on LINUX that generates a self-extractable compressed tar archive from a directory. The resulting file appears as a shell script, and can be launched as is. The archive will then uncompress itself via a temporary directory and an optional arbitrary command will be executed (for example an installation script). This is pretty similar to archives generated with "makeself". **tom** archives also checksums for integrity self-validation (MD5 checksums).

## LICENSE
**tom** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ tom --help
tom - Copyright (c) 2018, Michel RIZZO. All Rights Reserved.
tom - Version 1.0.0

A command-line utility to generate a self-extractable archive

Usage: tom [OPTIONS]...

  -h, --help              Print help and exit
  -V, --version           Print version and exit
  -o, --output=FILENAME   Application Archive name (result file)
  -n, --name=STRING       Application name
  -r, --revision=STRING   Application revision
  -d, --date=STRING       Application date
  -p, --path=PATH         Application path
  -t, --text=STRING       Short application description text
  -l, --license=FILENAME  License text file
  -f, --free              Free license  (default=off)
  -s, --script=FILENAME   Post-install script to be executed within the
                            installation directory

Exit: returns a non-zero status if an error is detected.

$ tom -V
tom - Copyright (c) 2018, Michel RIZZO. All Rights Reserved.
tom - Version 1.0.0
$ tom --output ~/release/TOM-1.0 --name "TOM" --revision "1.0.0" --date "25/09/2018" --path ~/Sandbox/tom --text "tom, the command-line utility to generate a self-extractable archive." --license License.txt --free --script postinstall.sh
...
$ tom -o ~/release/TOM-1.0 -n "TOM" -r "1.0.0" -d "25/09/2018" -p ~/Sandbox/tom -t "tom, the command-line utility to generate a self-extractable archive." -l License.txt -f -s postinstall.sh
...

```
**Note:** During the installation process of the application via the archive, the license will be displayed and an agreement will be required before the actual installation. The *--free* option indicates that the application is under a free world license. The license will be displayed for information and no agreement will be requested.

## EXAMPLE
![Example 1](./README_images/tom01.png  "Self-extractable archive generation")

On another computer, later...

![Example 2](./README_images/tom02.png  "Package installation")

## STRUCTURE OF THE APPLICATION
This section walks you through **tom**'s structure. Once you understand this structure, you will easily find your way around in **tom**'s code base.

``` bash
$ yaTree
./                  # Application level
├── README_images/  # Images for documentation
│   ├── tom01.png   # 
│   └── tom02.png   # 
├── src/            # Source directory
│   ├── Makefile    # Makefile
│   ├── tom.c       # Main program
│   └── tom.ggo     # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── COPYING.md      # GNU General Public License markdown file
├── LICENSE.md      # License markdown file
├── Makefile        # Makefile
├── README.md       # ReadMe markdown file
├── RELEASENOTES.md # Release Notes markdown file
└── VERSION         # Version identification text file

2 directories, 11 files
$ 
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd tom
$ make clean all
```

## HOW TO INSTALL THIS APPLICATION
```Shell
$ cd tom
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
```

## SOFTWARE REQUIREMENTS
- For usage, nothing particular...
- For development:
   - *GengetOpt* binary package installed, version 2.22.6.
   - *[libTom](https://gitlab.com/mubunt/libtom)*, a C library for LINUX to generate a self-extractable archive.
- Developped and tested on XUBUNTU 19.10, GCC v9.2.1.

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***